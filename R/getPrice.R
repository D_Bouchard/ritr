#' BID-ASK of a ticker
#'
#' Obtain the BID and the ASK of a ticker
#' @param s as List de connexion
#' @param ticker as string - titre
#'
#' @return BID as double \\
#'  ASK as double
#' @export
#'
#' @examples
#' crazy <- getPrice(s,"CRZY")
#' spread <- crazy$ask - crazy$bid
getPrice <- function(s, ticker){
  tryCatch({
    request <- httr::GET(paste0("http://localhost:9999/v1/securities/book?ticker=",ticker,"&limit=1&key=",s$apikey))
    book <- httr::content(request)

    if (httr::status_code(request)==200){
        if(getStatus(s)=="ACTIVE"){
            return(list(bid = book$bids[[1]]$price, ask = book$asks[[1]]$price))
        }
        else{
            message("Status: Stopped")
        }

    }
    else {
        print(paste("Error in getPrice():",httr::status_code(request)))
    }

  }, error=function(error_message){
        message("Error in getPrice()")
  })
}

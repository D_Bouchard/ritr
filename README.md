# Rest API pour RIT
Création d'un package R pour utiliser un REST API sur l'application Rotman Interactive Trader. <br />
Le package permet de faciliter le lien entre la programmation et la création d'algorithmes pour les étudiants.

## Pre-requis
```bash 
> install.packages("devtools")
> library(devtools)
> install_git("https://gitlab.com/D_Bouchard/ritr.git")
```

## Prochaines updates
### 0.2.0
1. ~~Verification du status de la simulation~~
2. ~~Ajout de la vérification sur getPrice, getTick, getVolume~~
3. ~~submitOrder() market or limit~~
4. ~~getOrder()~~
5. ~~cancelAllOrder()~~
<br />

### 0.3.0
0. ~~getSecurities -  Gets a list of available securities and associated positions.~~
1. ~~getPeriod() return {period, ticksPerPeriod, totalPeriod}~~
2. getTrader() return {traderID, firstName, lastName, nlv}
3. ~~getNews() return {news_id, period, tick, ticker, headline, body}~~
<br />


### 0.4.0
0. ~~getTender() Obtient l'info du tenders~~
1. ~~submitTender() Obtient l'info du tenders~~
2. ~~getPortfolio() Obtient l'info des titres détenus~~


## 0.6.0
0. infoLease() Information sur les assets
1. lease() Activer un asset
2. unlease() Désactiver un asset
3. useLease() Utiliser un asset

### 0.7.0
1. getVWAP() Return the vwap of the period
2. add volume to getHistory()

### 1.0.0

1. MA




#' Cancel all orders
#'
#' Cancel all order, but we can specify the ids or the ticker
#' Only one query parameter must be specified (ids or ticker)
#'
#'
#' @param s connexion status
#' @param ids Cancel a set of orders referenced via a comma-separated list of order ids.
#' @param ticker Cancel all open orders for a security.
#'
#' @export
#'
#' @examples
cancelAllOrder <- function(s, ids = NULL, ticker = NULL){
    tryCatch({
          if(is.null(ids)&& is.null(ticker)){

              post <- httr::POST(paste0("http://localhost:9999/v1/commands/cancel?all=1&key=",s$apikey))

          }else{
              if(!is.null(ids)){

                  if(length(ids)>1){

                      searchString <- ", "
                      replacementString <- "%2C"
                      ids <- gsub(searchString,replacementString,toString(ids))

                  }
                  post <- httr::POST(paste0("http://localhost:9999/v1/commands/cancel?all=0&ids=",ids,"&key=",s$apikey))

              }else if(!is.null(ticker)){

                  post <- httr::POST(paste0("http://localhost:9999/v1/commands/cancel?all=0&ticker=",ticker,"&key=",s$apikey))

              }

          }

    },error = function(error_message){

        message("Connexion error: APIKEY needed")

    })
}
